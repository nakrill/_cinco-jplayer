// Mp3 player skin
if (typeof zeSkins.jplayer == "undefined") {
	zeSkins.jplayer = function() {

		// this holds the GalleryObject, this object will be used by the skin to interact with the framework
		this.go = null;

		// defines groups of args that later will be used by the editor menu (the editor that you saw in step 2)
		this.arg_groups = {
			basic: { name: "Basic", desc: "Basic settings for the gallery" },
			player: { name: "Advanced", desc: "Audio player settings" },
                        design: { name: "Design", desc: "Player design settings" }
		};
		// this is the list of args that users will be able to change using the editor
		this.arg_map = {
			folder_name: { group: "basic", type: "text", name: "Gallery Name", desc: "Set your gallery name" },
			widget_max_width: { group: "basic", type: "dim", name: "Max Size of the Widget", desc: "When left empty a defualt value of 100% is assumes, the player will try to fit the availalble width size in your page or column. set this value to limit the max size of the player (for example 500, or 70%). For best results your page should be responsive too." },
			playlist_height: { group: "basic", type: "num", name: "Playlist Height", desc: "Playlist Height" },
			//widget: { group: "basic", type: "size", name: "Size", desc: "Size of the gallery" },
			
			css_main: { group: "player", type: "css", name: "Main CSS", desc: "Customize the CSS of your gallery" },

			volume: { group: "player", type: "num", name: "Volume", desc: "Set preset volume from  0-100"},
			theme_color: { group: "design", type: "color", name: "Theme Color", desc: "Customize the theme color of your gallery"},
			text_color: { group: "design", type: "color", name: "Text Color", desc: "Customize the text color of your gallery" },
			bg_color: { group: "design", type: "color", name: "Background Color", desc: "Customize the background color of your gallery" },


			autoStart: { group: "player", type: "bool", name: "Auto start", desc: "Start playback automatically on load" },
			pause_others:{ group: "player", type: "bool", name: "Pause other players", desc: "When set to On, all other players on the page will be paused when this player is in play (note : will only work if iframe is set to Off" },
			autoResume: { group: "player", type: "bool", name: "Continue on reload", desc: "Continue playback at current position after page reload" },
			autoResume_diff: { group: "player", type: "bool", name: "Continue playing on different pages", desc: "Continue playback at current position after page reload, you can embed gallery for example in the footer and it will play for whole site" },
			loopPlaylist: { group: "player", type: "bool", name: "Auto repeat of playlist", desc: "Auto repeat of playlist" },
			loopItem: { group: "player", type: "bool", name: "Auto repeat of song", desc: "Auto repeat of playing song" },
			showPlaylist: { group: "player", type: "bool", name: "Show playlist", desc: "Show playlist" },
			add_text:{ group: "player", type: "list",values:{"title":"Title","desc":"Description", "titleanddesc":"Title and Description"}, name: "Playlist Text", desc: "Playlist Text" },
			showShuffle: { group: "player", type: "bool", name: "Show Shuffle", desc: "Show Shuffle" },
			showSlider: { group: "player", type: "bool", name: "Show progress bar", desc: "Show progress bar" },
			showNav: { group: "player", type: "bool", name: "Show navigation", desc: "Show navigation buttons" },
			showVolume: { group: "player", type: "bool", name: "Show volume", desc: "Show volume buttons" },
			use_cover_image: { group: "player", type: "bool", name: "Cover Image", desc: "Show Cover Image" },
			image_in_playlist:{ group: "player", type: "list",values:{"none":"Disabled","small":"Small", "medium":"Medium", "large":"Large"},name: "Image in playlist", desc: "Image in playlist" },
			cover_image_max_height: { group: "player", type: "num", name: "Cover image max height", desc: "Cover image max height" }
			
		};
		// default values for each arg
		this.arg_defaults = {
			widget_w: 600,
			widget_h: 400,
			playlist_height:200,
			autoStart: false,
			pause_others:true,
			volume:60,
			autoResume: true,
			autoResume_diff:false,
			showPlaylist: true,
			add_text:'title',
			showSlider: true,
			showNav: true,
			showVolume: true,
			loopPlaylist:true,
			loopItem:false,
			showShuffle:false,
			use_cover_image:false,
			image_in_playlist:'none',
			cover_image_max_height:500
		};

		///// Skin methods (required interface for framework interaction) /////

		// The entry point for the skin :
		// - skin should load whatever external css, js file that is needed
		// - must call this.go.loadArgs for the go (GalleryObject) to process args
		// - optional - call this.go.getMediaJSON to load gallery media items
		this.init = function(go) {
			this.state = this.states.init;
			this.trace("Init starting");

			this.go = go;

			// init GO arguments
			this.arg_groups = this.go.merge_json(this.arg_groups, this.go.arg_misc_group);
			this.arg_map = this.go.merge_json(this.arg_map, this.go.arg_misc);
			this.go.loadArgs(this.arg_map, this.arg_defaults);
			
			// load javascript
			var scriptsArray = ['libs/zequery-1.9.1.min.js', 'libs/zequery.tmpl.min.js', 'libs/zequery.jplayer.min.js', 'libs/zequery.cookie.js', 'libs/zequery.simple-slider.min.js'],
				that = this;
			this.go.loadSkinJSSequence(scriptsArray, function() {
				that.trace("All scripts loaded");

				// Release $ variable
				if(!window.myzejQueryRocks){
					myzejQueryRocks = jQuery.noConflict(true);
				}
				that.onJQueryLoaded(myzejQueryRocks);
				// Load data
				that.loadItems();
			});
			this.trace("Init complete");
		};

		// Editor will call this method for every real time change in one of the args, reload etc.
		// the skin should implement a reload of the gallery.
		this.onEditPanelChange = function(what) {
			this.trace("onEditPanelChange fired: " + what + " has been changed.");
			this.state = this.states.init;
			this.loadItems();
		};

		// a callback for calling this.go.getMediaJSON, usually this is the last phase of the init
		// and not the skin should be able to create the gallery html
		this.onMediaJSON = function(json) {
			this.state = this.states.loaded;
			this.trace("Data loaded, starting gallery");
			this.go.MediaJSON = json;
			this.start();
		};

		///// Internal methods /////

		this.getUniqueId = function() {
			if (typeof this._counter === 'undefined') this._counter = 0;
			return ++this._counter;
		};

		// Sets a local scope for $ variable
		this.onJQueryLoaded = function($) {

			// Load items from server
			this.loadItems = function() {
				this.state = this.states.loading;
				this.trace("Loading data");

				var load_thumb,load_content;
				if(window.innerWidth < 800){ 
					load_content = "xlarge";
					load_thumb	= "small";
				}else{
					load_content = "xlarge";
					load_thumb	= "medium";
				}
				//var load_content = "null,a:mp3"
				load_content = "original,v:mp4_hd,a:mp3,p:" + load_content;
				// Retrieve items from server
				this.go.getMediaJSON({
					thumb: load_thumb,
					content: load_content
				});
			};

			// Generate and set html content
			this.start = function() {
				var that = this;
				if (!this.isGalleryLoaded()) return;
				this.state = this.states.rendering;
				this.trace("Starting");
				var id = this.go.loaderParams["_object"];
				var items = this.go.MediaJSON.items;
				this.titlesArray = [];
                                var counter=0;
				items = items.filter(function(element,i){
					if (element.content_type.indexOf('audio') > -1){
						element.el_index = counter;
						counter++;
						return true;
					}else if(element.content_type.indexOf('image') > -1){
						that.titlesArray.push({
							'src':element.content_url,
							'thumb':element.thumbnail_url
						});
					}
				});                               
				if (items.length == 0) {
					items = [{ title: "audio files not found"}];
				}
//                                if(items[0].duration != '') {                                    
//                                    this.firstAudioDuration = items[0].duration;                                    
//                                }
				if( this.titlesArray.length == 0){
					//this.go.args.use_cover_image = false;
					//this.go.args.image_in_playlist = 'none';
				}else if(this.titlesArray.length == 1){
					this.use_same_image = true;
				}else{
					this.use_same_image = false;	
				}
				
				var markup;
				if(this.go.args.image_in_playlist != 'none'){
					var imgStyle = 'display:inline-block;vertical-align: middle;margin:2px 5px 2px 2px;';
					if(this.go.args.image_in_playlist == 'small'){
						imgStyle += 'width:40px;height:30px';
					}else if(this.go.args.image_in_playlist == 'medium'){
						imgStyle += 'width:80px;height:60px';
					}else{
						imgStyle += 'width:110px;height:100px';
					}
					markup = "<li><a class='audioItem' href='${$item.correctUrl()}' ><span class='playlistThumb' style='"+imgStyle+"'><img  style='max-width: 100%;max-height: 100%;}' src='${$item.correctThumb()}'/></span><span class='audioItem_title'>${$item.correctTitle()}</span><span class='audioItem_desc' style='display:block;font-size:11px;'>${$item.correctDesc()}</span></a></li>";
				}else{
					markup = "<li><a class='audioItem' href='${$item.correctUrl()}' ><span class='audioItem_title'>${$item.correctTitle()}</span><span class='audioItem_desc' style='display:block;font-size:11px;'>${$item.correctDesc()}</span></a></li>";
				}
				
				$.template("skinTemplate", markup);

				var $playlist = $('<ul />');
				$.tmpl("skinTemplate", items,{
					  myValue: "content_url",
					  correctUrl: function(e) { 
						  return this.data.content_url.replace(/["'()]/g,"")+'&range=yes';
					  },
					  correctDesc:function(){
						 if(that.go.args.add_text == 'desc' || that.go.args.add_text == 'titleanddesc'  ){
							var returnDesc =  this.data.description.replace(/<br>/g, "\r\n").replace(/<br \/>/g, "\r\n");
                                                         
                                                        if($.trim(returnDesc) == '' && that.go.args.add_text == 'desc'){
                                                            return this.data.title;
                                                        }else{
                                                            return returnDesc;
                                                        }
						 }else{
							return '';
						 }
					  },
					  correctTitle:function(){
						 if(that.go.args.add_text != 'desc'  ){
							return  this.data.title;		 
						 }else{
							return '';
						 }
					  },
					  correctThumb: function() {
						if(that.use_same_image){
							return  that.titlesArray[0].thumb;
						}else if(typeof  that.titlesArray[this.data.el_index] != 'undefined'){
							return  that.titlesArray[this.data.el_index].thumb
						}else{
							return that.go.skinPath+'images/nocover.jpg'
						}
					  }
				}).appendTo($playlist);
				
				this.galleryId = 'gallery' + this.getUniqueId()+id;
				this.go.setGalleryHTML(this.go.getCSSCode("main") + "<div id='" + this.galleryId + "'>" + this.playerHtml + "</div>");
				if(this.go.args.use_cover_image && this.go.args.cover_image_max_height != ''){
					$('.jplayer', this.$gallery).css({
						'overflow':'hidden',
						'max-height': parseInt(this.go.args.cover_image_max_height),
						'height': parseInt(this.go.args.cover_image_max_height)
					});
				}
				this.go.trackEvent('Gallery View');
				if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB10|IEMobile|Opera Mini/i.test(navigator.userAgent) == true ) {
					$('#'+id).addClass('mobileBrowser');
				}else{
					$('#'+id).addClass('desktopBrowser');
				}
				$('#'+id+' .jp-playlist').append($playlist);
				this.$gallery = $('#' + this.galleryId).addClass('v2');
				var  presetVolume   =  isNaN(parseInt(this.go.args.volume)) ? 100: parseInt(this.go.args.volume);
				
				if(presetVolume > 100) presetVolume=100;
				else if(presetVolume < 0) presetVolume = 0;
				
				this.initPlayer(presetVolume);
				
				this.$volumeSlider = $('#'+id+' .volumeSlider');
				this.$seekSlider = $('#'+id+' .seekSlider');
				
				
				var tempcssValues =  this.$gallery.css('list-style-image').replace(')','').replace(/\"/g, "");
				tempcssValues = tempcssValues.split('&');
				var cssValues = {};
				for(var ind= 1; ind <tempcssValues.length; ind++){
					var tval = tempcssValues[ind].split('=');
					cssValues[tval[0]] = tval[1];
				}
				
				var volSliderDirection =  cssValues['cincopa-volume'];
				this.$gallery.addClass('cincopa-volume-'+volSliderDirection);
				this.$volumeSlider.simpleSlider({theme:'volume',value: presetVolume/100, direction:volSliderDirection});
				this.$volumeSlider.data('sliderObject').setValue(presetVolume/100);
				//trick to set volume dragger to 100%;
				this.$volumeSlider.prev('.slider-volume').find('.dragger').css('left',presetVolume+'%');
				
				
				this.$seekSlider.simpleSlider({theme:'volume seekBar',value:0});
				
				$('#'+id+' .jp-volume-max').bind('click',function(){
					 that.$volumeSlider.data('sliderObject').setValue(1)		
				}); 
			    this.$volumeSlider
				.bind("slider:ready slider:changed", 
					function (event, data) {
							that.player.jPlayer( "volume", data.value.toFixed(3));
				});
				var fixFlash_mp4_id;
				this.$seekSlider
				.bind("slider:ready slider:changed", 
					function (event, data) {
							that.ignore_timeupdate = true;
							var sp = that.player.data('jPlayer').status.seekPercent;
							if(sp > 0) {
								// Move the play-head to the value and factor in the seek percent.
								var t = data.value.toFixed(3)*100;
								that.player.jPlayer("playHead", t);
							} else {
								// Create a timeout to reset this slider to zero.
								setTimeout(function() {
									that.$seekSlider.data('sliderObject').setValue(0);
								}, 0);
							}
							clearTimeout(fixFlash_mp4_id);
							fixFlash_mp4_id = setTimeout(function() {
									that.ignore_timeupdate = false;
							},1000);
				}); 

				this.state = this.states.rendered;
				this.trace("Gallery render complete!");
				var mx_width = '100%';
				var usePercentage  = false;
				if (this.go.args.widget_max_width != '') {
					if(this.go.args.widget_max_width.indexOf('%') != -1){
						usePercentage = true;
						mx_width = this.go.args.widget_max_width;
					}else{
						mx_width = this.go.args.widget_max_width + 'px';
					}
				}
				
				$('#inner_'+id).css({
						'width':100+'%',
						'height':'auto',
						'max-width': (usePercentage || this.go.args.iframe) ? '100%': mx_width
					});
				$('#'+id).css({
					'max-width': (usePercentage && this.go.args.iframe) ? '100%': mx_width
				});
				if(this.go.args.iframe){
					parent.document.getElementById('zeiframe_'+this.go.args.id).width = '100%';
					parent.document.getElementById('zeiframe_'+this.go.args.id).style.width = '100%';
					parent.document.getElementById('zeiframe_'+this.go.args.id).style.maxWidth = mx_width;
				}
				if(that.go.args.iframe){
					winSelector = parent.window;
				}else{
					winSelector = window;
				}
				$(winSelector).bind('resize',function(){
					that.onResize();
					console.log('win resize')
				});
			};
			this.frameResize = function(){
				var id = this.go.loaderParams["_object"];
				var h = document.getElementById(this.galleryId).offsetHeight + 'px';
			    parent.document.getElementById('zeiframe_'+this.go.args.id).height =  h;
				parent.document.getElementById('zeiframe_'+this.go.args.id).style.height = h;
				console.log('frame resize');
				
			}
			// Init jPlayer and bind audio links
			this.initPlayer = function(preVol) {
				var that = this, go = that.go;
				var id = this.go.loaderParams["_object"];
				this.player = $('.jplayer', this.$gallery).jPlayer({
					swfPath: go.skinPath + "libs",
					noConflict:'myzejQueryRocks',
					supplied: "mp3",
					wmode:'window',  // to solve problem  on windows safari
					cssSelectorAncestor: "#" + this.galleryId + " .jp-container",
					volume: preVol,
                                        preload:'none',
					size:{
						width:'100%',
						height:'auto'
					},
					ready: function() {
						that.playerReady = true;
						that.trace('jPlayer ready.');
						
						/*set first track */
						var firstHref = $('.jp-playlist ul li:first a', that.$gallery).attr('href');  
						//$(this).jPlayer('setMedia', { mp3: firstHref, poster:  ''});
						/* end */
                                                /*if(that.firstAudioDuration)
                                                    $('.jp-duration', that.$gallery).html(that.firstAudioDuration.substr(0,8));*/
                                                
						if (go.args.autoResume && $.cookie('np_url_'+go.args.fid)) {
							that.play({
								url: $.cookie('np_url_'+go.args.fid),
								position: parseInt($.cookie('np_position_'+go.args.fid))
							});
						}else if (go.args.autoStart) {
                                                    $(this).jPlayer('setMedia', { mp3: firstHref, poster:  ''});
                                                    that.play();
                                                }else if(that.go.args.use_cover_image){
							var coverSrc = typeof  that.titlesArray[0] != 'undefined' ? that.titlesArray[0]['src']:that.go.skinPath+'images/nocover.jpg';
							$(this).jPlayer('setMedia', { mp3: firstHref, poster:  coverSrc});
							$('.jplayer > img', this.$gallery).one('load', function() {
								that.onResize();
							}).each(function() {
							  if(this.complete) $(this).load();
							});
						}
						if (go.args.autoResume) that.initAutoResume();
						
						that.onResize();
					},
					timeupdate: function(event) {
						if( event.jPlayer.status.paused && event.jPlayer.status.currentTime == 0){
							that.$seekSlider.prev('.seekBar').find('.dragger').css('left','0%')
						}else if(that.$seekSlider.data('sliderObject') && !that.ignore_timeupdate){
							that.$seekSlider.prev('.seekBar').find('.dragger').css('left',event.jPlayer.status.currentPercentAbsolute+'%')
						}
					},
					error: function(event) { 
						$('.jpli-pause', that.$gallery).css('display','none');
						$('.jpli-play', that.$gallery).css('display','block');
						that.trace('jPlayer error: ' + event.jPlayer.error.type);
						
						if(that.go.args.iframe) that.frameResize();
					},
					play: function(event) {					
						$('.jpli-play', that.$gallery).css('display','none');
						$('.jpli-pause', that.$gallery).css('display','block');
						if(go.args.pause_others)
							$(this).jPlayer("pauseOthers"); 
						that.go.trackEvent('Gallery audio play');
						that.trace('Playing: ' + event.jPlayer.status.src);
						
						$(window).resize();
						if(that.go.args.iframe) that.frameResize();
						
					},
					pause:function(){
						$('.jpli-pause', that.$gallery).css('display','none');
						$('.jpli-play', that.$gallery).css('display','block');
					},
					ended: function(event) { 						
						// loop item when end playing
						if(that.go.args.loopItem){
							that.play();							
						}else{
							that.playNext();
						} 
					}
				});
				$('.audioItem',this.$gallery).click(function(e) {
					that.trace('Audio item clicked.');
					e.preventDefault();
					that.selectItem($(this));
					that.play();
				});
				$('.jp-play', this.$gallery).click(function(e) { e.preventDefault(); that.play(); });
				$('.jp-next',this.$gallery).click(function(e) { e.preventDefault(); that.playNext(); });
				$('.jp-previous',this.$gallery).click(function(e) { e.preventDefault(); that.playPrev(); });
				
				$('.jpli-mute',this.$gallery).click(function(e) { $(this).hide();  $('.jpli-unmute',that.$gallery).css('display','block');    });
				$('.jpli-unmute',this.$gallery).click(function(e) { $(this).hide();  $('.jpli-mute',that.$gallery).css('display','block');  });
				
				
				$('.jpli-repeat',this.$gallery).click(function(e) { e.preventDefault();	that.go.args.loopItem = ! that.go.args.loopItem; $(this).toggleClass('on');     });
				$('.jpli-playlistbtn',this.$gallery).click(function(e) { e.preventDefault(); $('.jp-playlist', that.$gallery).slideToggle();  $(this).toggleClass('on');   });
				
				$('.jp-playlist', this.$gallery).toggle(go.args.showPlaylist);
	
				if(go.args.showPlaylist){
					$(this.$gallery).addClass('playlistvisible');
					$('.jpli-playlistbtn',this.$gallery).addClass('on');
				}
				
				$('.jp-slider-progress', this.$gallery).toggle(go.args.showSlider);
				$('.jp-previous,.jp-next', this.$gallery).toggle(go.args.showNav);
				$('.jpli-shuffle', this.$gallery).toggle(go.args.showShuffle);
				
				if(go.args.loopItem){
					$('.jpli-repeat',this.$gallery).addClass('on');				
				}
				if(go.args.showShuffle){
					$(this.$gallery).addClass('shufflevisible');					
					$('.jp-shuffle', this.$gallery).click(function(){
						$(this).parent().toggleClass('on');
					})
				}

				if(!go.args.showVolume){
					$('.jp-mute,.jp-unmute,.jp-volume-max,.jp-slider-volume-bar', this.$gallery).hide();
				}
			};
			this.onResize = function(){
				var id = this.go.loaderParams["_object"];
				var resizeContainerWidth;
				if( $('#'+id + ' .jp-audio').css('position') == 'fixed'){
					resizeContainerWidth = $(window).width();
				}else{
					resizeContainerWidth = $('#'+id).width();
				}
				if(resizeContainerWidth < 640){
					$('#'+id).addClass('mobileLayout');
				}else{
					$('#'+id).removeClass('mobileLayout');
				}
				$('.jplayer', this.$gallery).css('height','auto');
				if(this.go.args.iframe) this.frameResize();
			}
			this.initAutoResume = function() {
				var id = this.go.args.fid;
				var ms_counter = 0;
				var cookieOptions = {};
				if(this.go.args.autoResume_diff){
					cookieOptions = {path:'/'};
				}
				this.player.bind($.jPlayer.event.timeupdate, function(event) {
					// this event fires every 250 ms - update cookie every second
					if (ms_counter < 1000) ms_counter += 250;
					else {
						$.cookie('np_position_'+id, event.jPlayer.status.currentTime,cookieOptions);
						ms_counter = 0;
					}
				}).bind($.jPlayer.event.play, function(event) {
					// Store current song url in cookies to retrieve it after a page reload
					$.cookie('np_url_'+id, event.jPlayer.status.src,cookieOptions);
				}).bind($.jPlayer.event.pause, function() {
					// Clear cookies
					$.cookie('np_url_'+id, null,cookieOptions);
					$.cookie('np_positon_'+id, null,cookieOptions);
				});
			};

			// Selects a playlist item
			// $a: jquery (playlist hyperlink)
			this.selectItem = function($a) {
				if (!this.playerReady) return;
				var title = $a.find('.audioItem_title').html(),
				description = $a.find('.audioItem_desc').html(),
				that = this;
				
				this.trace('Playlist item selected: ' + title);
				this.$selectedItem = $a;

				$('li', this.$gallery).removeClass('jp-playlist-current');
				$a.closest('li').addClass('jp-playlist-current');
				$('.jp-title', this.$gallery).html(title);
				
				$('.jpli-currentinfo').html('<b>'+title+'</b><i>'+description+'</i>');
				var coverSrc ='';
				if(this.go.args.use_cover_image){
					var coverIndex;
					if(this.use_same_image){
						coverIndex = 0;
					}else{
						coverIndex = $a.closest('li').index();
					}
					coverSrc = typeof  this.titlesArray[coverIndex] != 'undefined' ? this.titlesArray[coverIndex]['src']:this.go.skinPath+'images/nocover.jpg';
					$('.jplayer', this.$gallery).css('height',parseInt(this.go.args.cover_image_max_height));
				}				
                                this.player.jPlayer('setMedia', { mp3: $a.attr('href'), poster: coverSrc});
				if(this.go.args.use_cover_image){
					$('.jplayer > img', this.$gallery).one('load', function() {
						$('.jplayer', this.$gallery).css('height','auto');	
					}).each(function() {
					  if(this.complete) $(this).load();
					});
				}
			};

			// Plays an mp3 file in jPlayer
			this.play = function(p) {
				if (!this.playerReady) return;
				if (p) {
					p = $.extend({ url: '', position: null }, p);
					this.selectItem($('a[href="' + p.url + '"]', this.$gallery));
				}
				if (!this.$selectedItem || this.$selectedItem.length === 0) {
					this.selectItem($(".jp-playlist a:first", this.$gallery));
				}
				this.player.jPlayer("play", p ? p.position : null);
			};

			this.playNext = function() {
				if (!this.playerReady) return;
				if (this.$selectedItem) {
					if(this.go.args.showShuffle && $('.jpli-shuffle', this.$gallery).hasClass('on') ){
						this.selectRandom(this.$selectedItem);
						return false;
					}
					if(this.$selectedItem.closest('li').next('li').length == 0  && this.go.args.loopPlaylist) {
						this.selectItem(this.$selectedItem.closest('ul').find('li:first a'));
					}else if(this.$selectedItem.closest('li').next('li').length == 0){
						this.player.jPlayer('stop');
						return false;
					}else{
						this.selectItem(this.$selectedItem.closest('li').next('li').find('a'));
					}
				}
				this.play();
			};
			this.playPrev = function() {
				if (!this.playerReady) return;
				if (this.$selectedItem) {
					if(this.go.args.showShuffle && $('.jpli-shuffle', this.$gallery).hasClass('on')){
						this.selectRandom(this.$selectedItem);
						return false;
					}
					if(this.$selectedItem.closest('li').prev('li').length == 0  && this.go.args.loopPlaylist) {
						this.selectItem(this.$selectedItem.closest('ul').find('li:last a'));
					}else if(this.$selectedItem.closest('li').prev('li').length == 0){
						this.player.jPlayer('stop');
						return false;
					}else{
						this.selectItem(this.$selectedItem.closest('li').prev('li').find('a'));
					}
				}
				this.play();
			}
			
			this.selectRandom  = function($a){
				var count = $(".jp-playlist ul li", this.$gallery).length;
				var random = Math.floor(Math.random()*count);
				var randElem = $a.parents('ul').find('li').eq(random);
				if($a.parent().index() == random){
				   this.selectRandom($a);
				   return false;
				}
				this.selectItem( randElem.find('a'))
				this.play();
			}

			// Gets an object from go.MediaJSON.items by item id
			this.getItemData = function(id) {
				var res = {};
				if (!this.isGalleryLoaded()) return res;
				var that = this;
				$.each(this.go.MediaJSON.items, function() {
					if (this.id === id) { res = this; return false; }
				});
				that.trace('Item id=' + id + ', data=' + JSON.stringify(res));
				return res;
			};

			// Checks if gallery items are loaded
			this.isGalleryLoaded = function() { return !!(this.go && this.go.MediaJSON); };
		}
//style="height:350px;"
		this.playerHtml = 	'<div class="jplayer"></div> \
								<div class="jp-container jp-audio"> \
									<div class="jp-type-playlist"> \
										<div class="jp-gui jp-interface"> \
										  <div class="jp-controls-wrap"> \
											<ul class="jp-controls"> \
												<li class="jpli-previous"><a href="javascript:;" class="jp-previous" tabindex="1">previous<i></i></a></li> \
												<li class="jpli-play"><a href="javascript:;" class="jp-play" tabindex="1">play<i></i></a></li> \
												<li class="jpli-pause"><a href="javascript:;" class="jp-pause" tabindex="1">pause<i></i></a></li> \
												<li class="jpli-stop"><a href="javascript:;" class="jp-stop" tabindex="1">stop<i></i></a></li> \
												<li class="jpli-next"><a href="javascript:;" class="jp-next" tabindex="1">next<i></i></a></li> \
												<li class="jpli-progress"><div class="jpli-progress-wrap"> \
													<div class="jp-current-time"></div> \
													<div class="jp-info-progress"> \
														<div class="jpli-currentinfo" style="display:none;"></div> \
														<div class="jp-slider-progress"><input type="text" style="display:none" class="seekSlider dataslider"  value="0" data-slider-highlight="true" data-slider-theme="volume"></div> \
													</div> \
													<div class="jp-duration"></div> \
												</div></li> \
												<li class="jpli-playlistbtn"><a href="javascript:;" class="jp-playlistbtn" tabindex="1" title="Playlist">playlist<i></i></a></li> \
												<li class="jpli-shuffle"><a href="javascript:;" class="jp-shuffle" tabindex="1" title="Shuffle">shuffle<i></i></a></li> \
												<li class="jpli-repeat"><a href="javascript:;" class="jp-repeat" tabindex="1" title="Repeat">repeat<i></i></a></li> \
												<li class="jpli-mute"><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute<i></i></a></li> \
												<li class="jpli-unmute"><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute<i></i></a></li> \
												<li class="jpli-volume-bar"><div class="jp-slider-volume-bar"><input type="text" style="display:none" class="volumeSlider dataslider"  value="1" data-slider-highlight="true" data-slider-theme="volume"></div></li> \
												<li class="jpli-volume-max"><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume<i></i></a></li> \
												<li class="jpli-chromecast" style="display:none"><a href="javascript:void(0)">chromecast<i></i></a></li> \
												<li class="jp-branding" style="display:none"><a href="javascript:void(0)">  <img src="http://static.cincopa.com/_cms/design13/images/logo.png"></a></li> \
											</ul> \
										  </div> \
										</div> \
										<div class="jp-playlist"></div> \
										<div class="jp-no-solution"> \
											<span>Update Required</span> \
											To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>. \
										</div> \
									</div> \
								</div>';
//  
		// For debugging (todo - remove these fields)
		this.states = { init: "Initializing", loading: "Loading", loaded: "Loaded", rendering: "Rendering", rendered: "Rendered" };
		this.state = this.states.init;

		this.trace = function(msg) {
			window.trace("SKIN: " + this.state + '; ' + msg);
		};
	};
}
/* support filter function on IE8 */
if (!Array.prototype.filter)
{
  Array.prototype.filter = function(fun /*, thisp */)
  {
    "use strict";

    if (this === void 0 || this === null)
      throw new TypeError();

    var t = Object(this);
    var len = t.length >>> 0;
    if (typeof fun !== "function")
      throw new TypeError();

    var res = [];
    var thisp = arguments[1];
    for (var i = 0; i < len; i++)
    {
      if (i in t)
      {
        var val = t[i]; // in case fun mutates this
        if (fun.call(thisp, val, i, t))
          res.push(val);
      }
    }

    return res;
  };
}